#include "ApplicationEnvironmentFactory.hpp"

int main(int argc, char* argv[])
{
    auto appEnv = ue::createApplicationEnvironment(argc, argv);
    auto& logger = appEnv->getLogger();
    logger.logInfo("Started: ", appEnv->getMyPhoneNumber());
    appEnv->startMessageLoop();
    logger.logInfo("Stopped: ", appEnv->getMyPhoneNumber());
}

